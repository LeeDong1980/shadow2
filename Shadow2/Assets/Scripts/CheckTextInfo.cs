﻿using UnityEngine.UI;
using UnityEngine;

public class CheckTextInfo : MonoBehaviour
{
    public Text Text1, Text1S;
    public Text Text2, Text2S;
    public GameObject playerControllerScript;

    private int p1Hp, p2Hp;
    private int p1Flashbang, p2Flashbang;
    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        p1Hp = playerControllerScript.GetComponent<PlayerController>().oneHealth;
        p2Hp = playerControllerScript.GetComponent<PlayerController>().twoHealth;
        p1Flashbang = playerControllerScript.GetComponent<PlayerController>().oneFlashbangAmount;
        p2Flashbang = playerControllerScript.GetComponent<PlayerController>().twoFlashbangAmount;

        Text1.text = Text1S.text = "P1\n影子: " + p1Hp + "\n閃光彈: " + p1Flashbang;
        Text2.text = Text2S.text = "P2\n影子: " + p2Hp + "\n閃光彈: " + p2Flashbang;
    }
}
