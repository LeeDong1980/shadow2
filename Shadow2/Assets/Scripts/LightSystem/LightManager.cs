﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour
{
    List<BasicLight> currentLights = new List<BasicLight>();
    List<PlayerShadow> currentShadows = new List<PlayerShadow>();

    List<BasicLight> lightsToRemove;
    List<PlayerShadow> shadowsToRemove;

    public float intensityFactor = 1f;
    public float distanceFactor = -0.1f;

    public BasicLight effectP1Light;
    public BasicLight effectP2Light;
    // Start is called before the first frame update
    void Start()
    {
        lightsToRemove = new List<BasicLight>();
        shadowsToRemove = new List<PlayerShadow>();
    }
    private void UpdateLights()
    {
        
        foreach (BasicLight light in currentLights)
        {
            if (light.toBeRemoved)
            {
                lightsToRemove.Add(light);
            }
            else
            {
                
                foreach (PlayerShadow shadow in currentShadows)
                {
                    

                    
                }
            }
        }

        

        foreach (BasicLight light in lightsToRemove)
        {
            currentLights.Remove(light);
        }
    }

    private void UpdateShadows()
    {
        BasicLight effectLight = null;
        float lightData = -Mathf.Infinity;
        foreach (PlayerShadow shadow in currentShadows)
        {
            if (shadow.toBeRemoved)
            {
                shadowsToRemove.Add(shadow);
            }
            else
            {
                foreach (BasicLight light in currentLights)
                {
                    //計算燈光強度及與目標距離
                    float distance = light.CalculatePlayerDistance(shadow);
                    float intensity = light.m_intensity;
                    float lightData_temp = distanceFactor * distance + intensityFactor * intensity;
                    if (lightData_temp > lightData)
                    {
                        lightData = lightData_temp;
                        effectLight = light;
                    }
                }
                //得出採用的燈光
                if (shadow.shadowType == PlayerShadow.ShadowType.Player1)
                {
                    effectP1Light = effectLight;
                    //照出影子
                    shadow.CastShadow(effectP1Light);
                }
                else if (shadow.shadowType == PlayerShadow.ShadowType.Player2)
                {
                    effectP2Light = effectLight;
                    //照出影子
                    shadow.CastShadow(effectP2Light);
                }
            }
        }
        foreach (PlayerShadow shadow in shadowsToRemove)
        {
            currentShadows.Remove(shadow);
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateLights();
        UpdateShadows();
    }

    public void RegisterLight(BasicLight light)
    {
        print("registering light:" + light.name);
        currentLights.Add(light);
    }
    public void RegisterShadow(PlayerShadow shadow)
    {
        print("registering shadow:" + shadow.name);
        currentShadows.Add(shadow);
    }
}
