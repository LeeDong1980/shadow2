﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShadow : MonoBehaviour
{

    protected LightManager manager;
    private GameObject shadow;

    public bool toBeRemoved = false;
    public float minShadowScale = 1;
    public float maxShadowScale = 3;
    public float basicScale = 1;
    public float dScale = 0.1f;
    public float closeRange = 1;
    //public float basicShadowX = 0;
    //public float basicShadowY = 0;
    //public float basicShadowOffsetX = 1;
    //public float basicShadowOffsetY = 1;
    public float maxShadowOffset = 1;
    //public float maxDir = 3;

    public float dPosX = 0.1f;
    public float dPosY = 0.1f;

    public enum ShadowType
    {
        Player1,
        Player2,
    }
    public ShadowType shadowType;


    public void CastShadow(BasicLight lightSource)
    {
        float dir_x = lightSource.m_dir.x;
        float dir_y = lightSource.m_dir.y;
        dir_x = (dir_x > 0) ? (Mathf.Clamp(lightSource.m_dir.x, 0, maxShadowOffset)) : (Mathf.Clamp(lightSource.m_dir.x, -maxShadowOffset, 0));
        dir_y = (dir_y > 0) ? (Mathf.Clamp(lightSource.m_dir.y, 0, maxShadowOffset)) : (Mathf.Clamp(lightSource.m_dir.y, -maxShadowOffset, 0));
        Vector2 dir = new Vector2(dir_x, dir_y);
        Vector2 dir_abs = new Vector2(Mathf.Abs(dir_x), Mathf.Abs(dir_y));
        float distance = Vector2.ClampMagnitude(dir_abs, maxShadowOffset).magnitude;
        float theta = Mathf.Atan2(dir.y, dir.x);
        float angle = theta * Mathf.Rad2Deg + 90;
        float dPosX_temp = (dir_x > 0) ? dPosX : -dPosX;
        float dPosY_temp = (dir_y > 0) ? dPosY : -dPosY;
        //float shadowOffsetX = (distance > closeRange) ? -Mathf.Cos(theta) * distance * dPosX_temp : 0;
        //float shadowOffsetY = (distance > closeRange) ? -Mathf.Sin(theta) * distance * dPosY_temp : 0;
        float shadowOffsetX = -Mathf.Cos(theta) * distance * dPosX_temp;
        float shadowOffsetY = -Mathf.Sin(theta) * distance * dPosY_temp;

        Vector2 shadowOffset = new Vector2(shadowOffsetX, shadowOffsetY);
        float shadowDistance = Vector2.Distance(shadowOffset, gameObject.transform.position);
        
        shadow.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        shadow.transform.localScale = new Vector3(basicScale, basicScale * Mathf.Clamp(lightSource.m_distance * dScale, minShadowScale, maxShadowScale), basicScale);
        shadow.transform.localPosition = new Vector2(shadowOffsetX, shadowOffsetY);
    }
    private void Awake()
    {
        manager = FindObjectOfType<LightManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        manager.RegisterShadow(this);
        shadow = transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
