﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalLight : BasicLight
{
    public float intensity;
    //public GameObject[] playerList = new GameObject[2];
    //private PlayerShadow [] playerShadowList = new PlayerShadow[2];
    NormalLight():base()
    {
        
    }

    public override float CalculatePlayerDistance(PlayerShadow player)
    {
        base.CalculatePlayerDistance(player);
        Vector2 playerPos = player.transform.position;
        m_distance = Vector2.Distance(this.gameObject.transform.position, playerPos);
        m_dir = this.gameObject.transform.position - player.transform.position;

        return m_distance;
    }

    public override void Display()
    {
        base.Display();

    }

    public override void CastPlayerShadow(PlayerShadow player, float distance)
    {
        base.CastPlayerShadow(player, distance);
        player.CastShadow(gameObject.GetComponent<NormalLight>());
    }

    // Start is called before the first frame update
    void Start()
    {
        manager.RegisterLight(this);
        //for (int i = 0; i < playerList.Length; i++)
        //{
        //    playerList[i] = GameObject.Find("player" + (i + 1).ToString());
        //    playerShadowList[i] = playerList[i].GetComponent<PlayerShadow>();
        //}
    }

    // Update is called once per frame
    void Update()
    {
        //for (int i = 0; i < playerList.Length; i++)
        //{
        //    CalculatePlayerDistance(playerList[i]);
        //    CastPlayerShadow(playerShadowList[i], m_distance);
        //}
        m_intensity = intensity;
    }

    public void RemoveLight()
    {
        toBeRemoved = true;
    }
}
