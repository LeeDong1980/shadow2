﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicLight : MonoBehaviour
{
    protected LightManager manager;
    public bool toBeRemoved { get; set; }
    public Vector2 m_position { get; set; }
    public float m_intensity { get; set; }
    public float m_distance { get; set; }
    public float m_range { get; set; }
    public Vector2 m_dir { get; set; }

    private void Awake()
    {
        manager = FindObjectOfType<LightManager>();
    }

    public virtual float CalculatePlayerDistance(PlayerShadow player) { return 0; }
    public virtual void Display() { }
    public virtual void CastPlayerShadow(PlayerShadow player, float distance) { }
}
