﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemBase : MonoBehaviour
{
    protected GameObject thisItem;
    protected Animator thisAnimator;
    protected bool thisTrigger = true;

    virtual public void Get(int playerID = 0)
    {
        if (!thisTrigger) return;
        thisTrigger = false;
        Destroy(thisItem, 2.5f);
    }
    virtual public void Update() 
    {
    }
    virtual public void Start()
    {
        thisItem = gameObject;
        thisAnimator = gameObject.GetComponent<Animator>();
    }
    virtual public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && thisTrigger)
        {
            switch (collision.name)
            {
                case "player1":
                    Get(1);
                    break;
                case "player2":
                    Get(2);
                    break;
                default:
                    break;
            }
        }
    }
}