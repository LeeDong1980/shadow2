﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharNext : MonoBehaviour
{
    public Button NextBtn;
    // Start is called before the first frame update


    void Start()
    {
        NextBtn.onClick.AddListener(Next);
    }

    // Update is ced once per frame
    void Update()
    {


    }

    void Next()
    {
        SceneManager.LoadScene("HowtoPlay");
    }

}


    