﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayNextCon : MonoBehaviour
{
    public Button NextBtn;
    // Start is called before the first frame update

    public Sprite initStateImg;
    public Sprite hoverStateImg;

    void Start()
    {

        NextBtn.onClick.AddListener(Next);

    }

    // Update is ced once per frame
    void Update()
    {


    }



    void Next()
    {
        GameObject.Find("ClickSound").GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("Ready");
    }



    public void MouseHover()
    {
        NextBtn.GetComponent<Image>().sprite = hoverStateImg;
    }

    public void MouseLeave()
    {
        NextBtn.GetComponent<Image>().sprite = initStateImg;
    }

}
