﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleStartCon : MonoBehaviour
{
    public Button StartBtn;
    // Start is called before the first frame update

    public Sprite initStateImg;
    public Sprite hoverStateImg;

    void Start()
    {

        StartBtn.onClick.AddListener(Next);

    }

    // Update is ced once per frame
    void Update()
    {


    }



    void Next()
    {
        GameObject.Find("ClickSound").GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("BattleScene");
    }



    public void MouseHover()
    {
        StartBtn.GetComponent<Image>().sprite = hoverStateImg;
    }

    public void MouseLeave()
    {
        StartBtn.GetComponent<Image>().sprite = initStateImg;
    }

}
