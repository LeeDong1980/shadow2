﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleManager : MonoBehaviour
{
    public GameObject[] itemToGenerate;

    public Vector3[] spawnPoint = new Vector3[50];
    public float generateTime = 10f;
    
    private int spawnPointCounter;

    [Header("還影子之門Prefab")]
    public GameObject door;

    private bool doorShoewed = false;

    void Start()
    {
        InvokeRepeating("GenerateItem", 5, generateTime);
        InvokeRepeating("CheckRoundStatus", 10, 1);
        spawnPointCounter = 0;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (spawnPointCounter >= spawnPoint.Length) return;
            spawnPoint[spawnPointCounter] = GameObject.Find("player1").transform.position;
            spawnPointCounter++;
        }
    }
    void GenerateItem()
    {
        if (spawnPoint.Length <= 0 || itemToGenerate.Length <= 0) return;
        int randomItem = Random.Range(0, itemToGenerate.Length);
        int randomSpawnPoint = Random.Range(0, spawnPoint.Length);
        Instantiate(itemToGenerate[randomItem], spawnPoint[randomSpawnPoint], Quaternion.identity);
    }
    void CheckRoundStatus()
    {
        int p1hp = GameObject.Find("PlayerController").GetComponent<PlayerController>().oneHealth;
        int p2hp = GameObject.Find("PlayerController").GetComponent<PlayerController>().twoHealth;
        if(p1hp == 1 && p2hp == 1 && !doorShoewed)
        {
            int randomSpawnPoint = Random.Range(0, spawnPoint.Length);
            Instantiate(door, spawnPoint[randomSpawnPoint], Quaternion.identity);
            doorShoewed = true;
        }
        if(p1hp == 0 || p2hp == 0)
        {

            SceneManager.LoadScene("BadEnd");
        }
    }
}
