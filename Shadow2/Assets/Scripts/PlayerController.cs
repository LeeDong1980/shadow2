﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public GameObject player1, player2;
    public GameObject player1Img, player2Img;
    public GameObject[] P1Costumes = new GameObject[3];
    public GameObject[] P2Costumes = new GameObject[3];
    public GameObject flashBang;
    public ParticleSystem flashBangExplosion;
    public GameObject flashBangLight;

    private Animator player1Anim, player2Anim;
    //private GameObject head1, body1, tail1, head2, body2, tail2;
    //public Sprite[] headImgPool = new Sprite[3];
    //public Sprite[] bodyImgPool = new Sprite[3];
    //public Sprite[] tailImgPool = new Sprite[3];
    private Rigidbody2D oneRigCharacter, twoRigCharacter;
    private Vector2 oneMoveVelocity, twoMoveVelocity;
    public GameObject umbrella;
    public GameObject cuff;


    public int oneHealth = 3, twoHealth = 3;
    public bool oneFacingLeft, twoFacingLeft;
    public float oneSpeed = 5.0f, twoSpeed = 5.0f;
    public float playerHitRange = 1f;

    [Header("P1數值")]
    public float oneSpeedTimes;
    public float oneCanAttackTimes;
    public int oneFlashbangAmount;
    public float oneGodModeTimes;
    public string attackLayerName1 = "P2Shadow";
    public GameObject umbrella1;
    public GameObject cuff1;

    [Header("P2數值")]
    public float twoSpeedTimes;
    public float twoCanAttackTimes;
    public int twoFlashbangAmount;
    public float twoGodModeTimes;
    public string attackLayerName2 = "P1Shadow";
    public GameObject umbrella2;
    public GameObject cuff2;

    [Header("道具使用秒數")]
    public float speedBoostSec;
    public float godModeSec;
    public float attackSec;
    //Debug用，之後可刪除

    public AudioSource atkSE;
    public AudioSource hurtSE;

    void Start()
    {
        //head1 = player1Img.transform.GetChild(0).gameObject;
        //body1 = player1Img.transform.GetChild(1).gameObject;
        //tail1 = player1Img.transform.GetChild(2).gameObject;
        //head2 = player2Img.transform.GetChild(0).gameObject;
        //body2 = player2Img.transform.GetChild(1).gameObject;
        //tail2 = player2Img.transform.GetChild(2).gameObject;

        //head1.GetComponent<SpriteRenderer>().sprite = headImgPool[TextureData.head1Id];
        //body1.GetComponent<SpriteRenderer>().sprite = bodyImgPool[TextureData.body1Id];
        //tail1.GetComponent<SpriteRenderer>().sprite = tailImgPool[TextureData.tail1Id];
        //head2.GetComponent<SpriteRenderer>().sprite = headImgPool[TextureData.head2Id];
        //body2.GetComponent<SpriteRenderer>().sprite = bodyImgPool[TextureData.body2Id];
        //tail2.GetComponent<SpriteRenderer>().sprite = tailImgPool[TextureData.tail2Id];

        player1Anim = player1Img.GetComponent<Animator>();
        player2Anim = player2Img.GetComponent<Animator>();
        player1Anim.SetInteger("Head", TextureData.head1Id + 1);
        player1Anim.SetInteger("Body", TextureData.body1Id + 1);
        player1Anim.SetInteger("Bot", TextureData.tail1Id + 1);
        player2Anim.SetInteger("Head", TextureData.head2Id + 1);
        player2Anim.SetInteger("Body", TextureData.body2Id + 1);
        player2Anim.SetInteger("Bot", TextureData.tail2Id + 1);

        oneRigCharacter = player1.GetComponent<Rigidbody2D>();
        twoRigCharacter = player2.GetComponent<Rigidbody2D>();
        InvokeRepeating("StatusCheckTimer", 5.0f, 1.0f);

        oneFacingLeft = twoFacingLeft = true;

        for (int i = 0; i < 3; i++)
        {
            P1Costumes[i] = player1Img.transform.GetChild(i).gameObject;
            P2Costumes[i] = player2Img.transform.GetChild(i).gameObject;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 oneMoveInput = new Vector2(Input.GetAxisRaw("player1X"), Input.GetAxisRaw("player1Y"));
        Vector2 twoMoveInput = new Vector2(Input.GetAxisRaw("player2X"), Input.GetAxisRaw("player2Y"));
        //Vector2 oneMoveInput = new Vector2(Input.GetAxis("joystick1X"), Input.GetAxis("joystick1Y"));

        if(oneFacingLeft && oneMoveInput.x > 0)
        {
            Flip(player1Img, 1);
        }else if(!oneFacingLeft && oneMoveInput.x < 0)
        {
            Flip(player1Img, 1);
        }

        if (twoFacingLeft && twoMoveInput.x > 0)
        {
            Flip(player2Img, 2);
        }
        else if (!twoFacingLeft && twoMoveInput.x < 0)
        {
            Flip(player2Img, 2);
        }

        oneMoveVelocity = oneMoveInput.normalized * oneSpeed * ((oneSpeedTimes > 0) ? 3f:1f) ;
        twoMoveVelocity = twoMoveInput.normalized * twoSpeed * ((twoSpeedTimes > 0) ? 3f:1f) ;

        if (oneMoveVelocity.x != 0 || oneMoveVelocity.y != 0)
        {
            player1Anim.SetBool("Walk", true);
        }else if(oneMoveVelocity.x == 0 && oneMoveVelocity.y == 0)
        {
            player1Anim.SetBool("Walk", false);
        }

        if (twoMoveVelocity.x != 0 || twoMoveVelocity.y != 0)
        {
            player2Anim.SetBool("Walk", true);
        }
        else if (twoMoveVelocity.x == 0 && twoMoveVelocity.y == 0)
        {
            player2Anim.SetBool("Walk", false);
        }

        if (Input.GetKeyDown(KeyCode.Space) && oneCanAttackTimes > 0)
        {
            TryToAttack(player1);
        }
        if (Input.GetKeyDown(KeyCode.KeypadEnter) && twoCanAttackTimes > 0)
        {
            TryToAttack(player2);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && oneFlashbangAmount > 0)
        {
            ThrowFlashBang(1);
        }
        if (Input.GetKeyDown(KeyCode.RightControl) && twoFlashbangAmount > 0)
        {
            ThrowFlashBang(2);
        }


        changeCloth();
        if (oneGodModeTimes > 0)
        {
            umbrella1.SetActive(true);
        }
        else
        {
            umbrella1.SetActive(false);
        }
        if (twoGodModeTimes > 0)
        {
            umbrella2.SetActive(true);
        }
        else
        {
            umbrella2.SetActive(false);
        }
    }
    private void FixedUpdate()
    {
        oneRigCharacter.MovePosition(oneRigCharacter.position + oneMoveVelocity * Time.deltaTime * 1.5f);
        twoRigCharacter.MovePosition(twoRigCharacter.position + twoMoveVelocity * Time.deltaTime * 1.5f);

        if (oneMoveVelocity.x != 0)
            oneFacingLeft = (oneMoveVelocity.x > 0) ? false : true;
        if (twoMoveVelocity.x != 0)
            twoFacingLeft = (twoMoveVelocity.x > 0) ? false : true;
    }

    void Flip(GameObject player, int type)
    {
        switch (type)
        {
            case 1:
                oneFacingLeft = !oneFacingLeft;

                Vector3 scale1 = player.transform.localScale;
                scale1.x *= -1;
                player.transform.localScale = scale1;

                break;
            case 2:
                twoFacingLeft = !twoFacingLeft;

                Vector3 scale2 = player.transform.localScale;
                scale2.x *= -1;
                player.transform.localScale = scale2;
                break;
            default:
                break;
        }
    }

    void TryToAttack(GameObject player)
    {

        switch (player.name)
        {
            case "player1":
                Collider2D hitCollider1 = Physics2D.OverlapCircle(player.transform.position, playerHitRange, 1 << LayerMask.NameToLayer("P2Shadow"));
                player1Anim.SetTrigger("Attack");
                atkSE.Play();
                cuff1 = Instantiate(cuff, player.transform);
                Destroy(cuff1, 0.5f);
                if (hitCollider1 != null)
                {
                    if (twoGodModeTimes > 0) return;
                    hurtSE.Play();
                    P2Costumes[twoHealth - 1].SetActive(false);
                    twoHealth -= 1;
                    twoGodModeTimes = 3;
                }
                break;
            case "player2":
                Collider2D hitCollider2 = Physics2D.OverlapCircle(player.transform.position, playerHitRange, 1 << LayerMask.NameToLayer("P1Shadow"));
                player2Anim.SetTrigger("Attack");
                atkSE.Play();
                cuff2 = Instantiate(cuff, player.transform);
                Destroy(cuff2, 0.5f);
                if (hitCollider2 != null)
                {
                    if (oneGodModeTimes > 0) return;
                    hurtSE.Play();
                    P1Costumes[oneHealth - 1].SetActive(false);
                    oneHealth -= 1;
                    oneGodModeTimes = 3;
                }
                break;
            default:
                break;
        }
    }
    void StatusCheckTimer()
    {
        oneSpeedTimes = (oneSpeedTimes > 0) ? (oneSpeedTimes - 1f) : 0f;
        twoSpeedTimes = (twoSpeedTimes > 0) ? (twoSpeedTimes - 1f) : 0f;
        oneCanAttackTimes = (oneCanAttackTimes > 0) ? (oneCanAttackTimes - 1f) : 0f;
        twoCanAttackTimes = (twoCanAttackTimes > 0) ? (twoCanAttackTimes - 1f) : 0f;
        oneGodModeTimes = (oneGodModeTimes > 0) ? (oneGodModeTimes - 1f) : 0f;
        twoGodModeTimes = (twoGodModeTimes > 0) ? (twoGodModeTimes - 1f) : 0f;

    }
    public void SpeedBoost(int playerID)
    {
        switch (playerID)
        {
            case 1:
                oneSpeedTimes += speedBoostSec;
                break;
            case 2:
                twoSpeedTimes += speedBoostSec;
                break;
            default:
                break;
        }
    }

    public void atkIcon()
    {
        
    }

    public void TurnIntoAttacker(int playerID)
    {
        switch (playerID)
        {
            case 1:
                oneCanAttackTimes += attackSec;
                break;
            case 2:
                twoCanAttackTimes += attackSec;
                break;
            default:
                break;
        }
    }
    public void TurnIntoGod(int playerID)
    {
        switch (playerID)
        {
            case 1:
                oneGodModeTimes += godModeSec;
                break;
            case 2:
                twoGodModeTimes += godModeSec;
                break;
            default:
                break;
        }
    }
    public void ThrowFlashBang(int playerID)
    {
        switch (playerID)
        {
            case 1:
                GameObject tempFlashBang = Instantiate(flashBang, player1.transform.position, Quaternion.identity);
                DestroyImmediate(tempFlashBang.GetComponentInChildren<Collider2D>());

                //移動閃光彈的位置到p2
                Vector3 dist = (player2.transform.position - player1.transform.position).normalized;
                Vector3 finalPos1 = player2.transform.position + dist * 100.0f;
                tempFlashBang.transform.DOMove(finalPos1, 0.8f).OnComplete(() => {
                    //此處為p1丟閃光彈爆炸後的function
                    ParticleSystem tempFlashBangExplosion1 = Instantiate(flashBangExplosion, new Vector3(finalPos1.x, finalPos1.y, -5.5f), Quaternion.identity);
                    tempFlashBangExplosion1.Play();
                    GameObject bangLight = Instantiate(flashBangLight, new Vector3(finalPos1.x, finalPos1.y, -5.5f), Quaternion.identity);
                    Destroy(tempFlashBang);
                    StartCoroutine(RemoveFlashLight(bangLight.GetComponent<NormalLight>()));
                    Destroy(bangLight, 3.0f);
                });
                oneFlashbangAmount--;
                break;
            case 2:
                GameObject tempFlashBang2 = Instantiate(flashBang, player2.transform.position, Quaternion.identity);
                DestroyImmediate(tempFlashBang2.GetComponentInChildren<Collider2D>());

                //移動閃光彈的位置到p1
                Vector3 dist2 = (player1.transform.position - player2.transform.position).normalized;
                Vector3 finalPos2 = player1.transform.position + dist2 * 100.0f;
                tempFlashBang2.transform.DOMove(finalPos2, 0.8f).OnComplete(() => {
                    //此處為p2丟閃光彈爆炸後的function
                    //flashBangExplosion.Play();
                    ParticleSystem tempFlashBangExplosion2 = Instantiate(flashBangExplosion, new Vector3(finalPos2.x, finalPos2.y, -5.5f), Quaternion.identity);
                    tempFlashBangExplosion2.Play();
                    GameObject bangLight = Instantiate(flashBangLight, new Vector3(finalPos2.x, finalPos2.y, -5.5f), Quaternion.identity);
                    Destroy(tempFlashBang2);
                    StartCoroutine(RemoveFlashLight(bangLight.GetComponent<NormalLight>()));
                    Destroy(bangLight, 3.0f);
                });
                twoFlashbangAmount--;
                break;
            default:
                break;
        }
    }

    IEnumerator RemoveFlashLight(NormalLight flashLight)
    {
        yield return new WaitForSeconds(1.5f);
        flashLight.RemoveLight();
    }
    public void GetFlashBang(int playerID)
    {
        switch (playerID)
        {
            case 1:
                oneFlashbangAmount++;
                break;
            case 2:
                twoFlashbangAmount++;
                break;
            default:
                break;
        }
    }
    public void changeCloth()
    {
        switch (oneHealth)
        {
            case 3:

                break;
            case 2:

                break;
            case 1:

                break;
            case 0:
                
                break;
        }

        switch (twoHealth)
        {
            case 3:

                break;
            case 2:

                break;
            case 1:

                break;
            case 0:

                break;
        }
    }
}

//火雞胸肉潛艇堡 201$ 20201212 Hackthon of NCCU
