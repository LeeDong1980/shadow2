﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorController : MonoBehaviour
{
    public bool p1Touch = false;
    public bool p2Touch = false;
    // Start is called before the first frame update
    void Start()
    {
        p1Touch = false;
        p2Touch = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (p1Touch && p2Touch)
        {
            SceneManager.LoadScene("GoodEnd");
        }
    }
    private void OnTriggerEnter2D(Collider2D collison)
    {
        if (collison.gameObject.name == "player1")
        {
            p1Touch = true;
                if (p1Touch && p2Touch)
            {
                SceneManager.LoadScene("GoodEnd");
            }
        }
        if (collison.gameObject.name == "player2")
        {
            p2Touch = true;
            if (p1Touch && p2Touch)
            {
                SceneManager.LoadScene("GoodEnd");
            }
        }
    }
}
