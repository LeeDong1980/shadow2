﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class hatcon : MonoBehaviour
{
    public enum ClothType {
        Head,
        Body,
        Tail,
    }
    public ClothType clothType; 

    public Button Dbtn;
    public Button Ubtn;
    public RawImage shirt1;
    public RawImage shirt1_P1;
    public RawImage shirt1_P2;
    public Texture2D[] shirtChanged;
    public Button Check;
    public Button Check2;
    public RawImage p1;
    public RawImage p2;
    public Texture2D p1_1;
    public Texture2D p2_1;
    public int shirtChoice;
    public int count = 0;
    int p = 1;

    // Start is called before the first frame update
    void Start()
    {
        Dbtn.onClick.AddListener(OnDownBtnClick);
        Ubtn.onClick.AddListener(OnUpBtnClick);
        Check.onClick.AddListener(OnCheck);
        Check2.onClick.AddListener(OnCheck);
        Check.interactable = true;
        Check2.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDownBtnClick()
    {
        count++;
        GameObject.Find("ClickSound").GetComponent<AudioSource>().Play();
        shirt1.texture = shirtChanged[(count % 3)];
        if (p == 1) { shirt1_P1.texture = shirtChanged[(count % 3)]; }
        else { shirt1_P2.texture = shirtChanged[(count % 3)]; }
    }

    void OnUpBtnClick()
    {
        count--;
        GameObject.Find("ClickSound").GetComponent<AudioSource>().Play();
        if (count > 0)
        {
            shirt1.texture = shirtChanged[(count % 3)];
            if (p == 1) { shirt1_P1.texture = shirtChanged[(count % 3)]; }
            else { shirt1_P2.texture = shirtChanged[(count % 3)]; }
        }
        else
        {
            count = 5;
            shirt1.texture = shirtChanged[(count % 3)];
            if (p == 1) { shirt1_P1.texture = shirtChanged[(count % 3)]; }
            else { shirt1_P2.texture = shirtChanged[(count % 3)]; }
        }
    }

    void OnCheck()
    {
        switch (p)
        {
            case 1:
                switch (clothType)
                {
                    case ClothType.Head:
                        TextureData.head1Id = count % 3;
                        break;
                    case ClothType.Body:
                        TextureData.body1Id = count % 3;
                        break;
                    case ClothType.Tail:
                        TextureData.tail1Id = count % 3;
                        break;
                    default:
                        break;
                }
                break;
            case 2:
                switch (clothType)
                {
                    case ClothType.Head:
                        TextureData.head2Id = count % 3;
                        break;
                    case ClothType.Body:
                        TextureData.body2Id = count % 3;
                        break;
                    case ClothType.Tail:
                        TextureData.tail2Id = count % 3;
                        break;
                    default:
                        break;
                }
                
                break;
            default:
                break;
        }
        if(p == 1){
            Check.interactable = false;
            Check2.interactable = true;
        }
        if(p == 2) SceneManager.LoadScene("HowToPlay");

        p = 2;
        count = 0;
        shirt1.texture = shirtChanged[(count % 3)];
        p1.texture = p1_1;
        p2.texture = p2_1;
        GameObject.Find("ClickSound").GetComponent<AudioSource>().Play();
    }
}
