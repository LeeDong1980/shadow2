﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartController : MonoBehaviour
{
    public Button StartBtn;
    public Sprite initStateImg;
    public Sprite hoverStateImg;

    void Start()
    {
        StartBtn.onClick.AddListener(StartGame);
    }

    public void StartGame()
    {
        GameObject.Find("ClickSound").GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("Story");
    }

    public void MouseHover() {
        StartBtn.GetComponent<Image>().sprite = hoverStateImg;
    }

    public void MouseLeave()
    {
        StartBtn.GetComponent<Image>().sprite = initStateImg;
    }

}
