﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class toTexture2D
{
    public static Texture2D to_Texture2D(this Texture texture)
    {
        return Texture2D.CreateExternalTexture(
            texture.width,
            texture.height,
            TextureFormat.RGB24,
            false, false,
            texture.GetNativeTexturePtr());
    }
}

public static class TextureData
{
    public static Texture2D Head1;
    public static Texture2D Body1;
    public static Texture2D Bottom1;
    public static Texture2D Head2;
    public static Texture2D Body2;
    public static Texture2D Bottom2;

    public static int head1Id;
    public static int body1Id;
    public static int tail1Id;
    public static int head2Id;
    public static int body2Id;
    public static int tail2Id;
}

public class RecordCon : MonoBehaviour
{
    public Button P1Checked;
    public Button P2Checked;
    public RawImage P1Head;
    public RawImage P1Shirt;
    public RawImage P1Bottom;
    public RawImage P2Head;
    public RawImage P2Shirt;
    public RawImage P2Bottom;
    public Rect[] rect1;
    public Texture2D[] Head;
    public Texture2D[] Shirt;
    public Texture2D[] Bottom;

    // Start is called before the first frame update
    void Start()
    {
        P1Checked.onClick.AddListener(OnP1Checked);
        P2Checked.onClick.AddListener(OnP2Checked);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnP1Checked()
    {
        Texture2D P1HeadTexture = toTexture2D.to_Texture2D(P1Head.texture);
        Texture2D P1ShirtTexture = toTexture2D.to_Texture2D(P1Shirt.texture);
        Texture2D P1BottomTexture = toTexture2D.to_Texture2D(P1Bottom.texture);

        TextureData.Head1 = P1HeadTexture;
        TextureData.Body1 = P1ShirtTexture;
        TextureData.Bottom1 = P1BottomTexture;
    }

    public void OnP2Checked()
    {
        Texture2D P2HeadTexture = toTexture2D.to_Texture2D(P2Head.texture);
        Texture2D P2ShirtTexture = toTexture2D.to_Texture2D(P2Shirt.texture);
        Texture2D P2BottomTexture = toTexture2D.to_Texture2D(P2Bottom.texture);

        TextureData.Head2 = P2HeadTexture;
        TextureData.Body2 = P2ShirtTexture;
        TextureData.Bottom2 = P2BottomTexture;
    }
}
