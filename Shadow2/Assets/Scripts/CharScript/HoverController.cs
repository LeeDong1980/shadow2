﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HoverController : MonoBehaviour
{
    public Sprite initStateImg;
    public Sprite hoverStateImg;
    public Button Btn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MouseHover()
    {
        Btn.GetComponent<Image>().sprite = hoverStateImg;
    }

    public void MouseLeave()
    {
        Btn.GetComponent<Image>().sprite = initStateImg;
    }
}
