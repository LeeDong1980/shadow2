﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollower : MonoBehaviour
{
    public GameObject player1;
    public GameObject player2;
    public float camZ;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 dir = player2.transform.position - player1.transform.position;
        gameObject.transform.position = player1.transform.position + new Vector3(dir.x / 2, dir.y / 2, camZ);
    }
}
