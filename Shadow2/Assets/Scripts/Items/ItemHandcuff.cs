﻿using UnityEngine;

public class ItemHandcuff : ItemBase
{
    public RuntimeAnimatorController m_animator;
    public override void Get(int playerID)
    {
        base.Get();
        thisAnimator.runtimeAnimatorController = m_animator;
        GameObject.Find("PlayerController").GetComponent<PlayerController>().TurnIntoAttacker(playerID);
    }
}
