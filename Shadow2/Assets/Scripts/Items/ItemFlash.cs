﻿using UnityEngine;
public class ItemFlash : ItemBase
{
    public RuntimeAnimatorController m_animator;
    public override void Get(int playerID)
    {
        base.Get();
        thisAnimator.runtimeAnimatorController = m_animator;
        GameObject.Find("PlayerController").GetComponent<PlayerController>().GetFlashBang(playerID);
    }
}
