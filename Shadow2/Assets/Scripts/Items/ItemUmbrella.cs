﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ItemUmbrella : ItemBase
{
    public RuntimeAnimatorController m_animator;
    public override void Get(int playerID)
    {
        base.Get();
        thisAnimator.runtimeAnimatorController = m_animator;
        GameObject.Find("PlayerController").GetComponent<PlayerController>().TurnIntoGod(playerID);
    }
}
